import java.awt.Dimension;
import java.util.Date;
import java.util.Locale;
import java.awt.Toolkit;

public class HoraDoSistema {
    public static void main(String[] args){

        
        Date relogio = new Date();
        Locale localizacao = Locale.getDefault(); 
        Dimension resolucao = Toolkit.getDefaultToolkit().getScreenSize();

        System.out.println("A hora do sistema: ");
        System.out.println(relogio.toString());
        System.out.println("Linguagem do sistema: ");
        System.out.println(localizacao + " " + localizacao.getDisplayCountry()); 
        //.getDisplayLanguage() imprime portugues .getLanguage() imprime pt .getCountry imprime Brasil.
        System.out.println("A resolucao da tela:");
        System.out.println(resolucao.width +" x "+ resolucao.height);
    }
    
}
